from mpi4py import MPI
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy
numpy.set_printoptions(threshold=numpy.nan)

#Mpi commands
comm = MPI.COMM_WORLD
rank = comm.rank
size = comm.size

import glob
#Get all the files form Gutenberg
text_files = glob.glob('Gutenberg/txt/*.txt')
ds_size = len(text_files)#Get the size from the dataset 
texts=[]
enc='latin-1'# Latin-1 is used for special character
send1 = []
send2 = []
send3 = []
send4 = []


localP = ds_size / 3 #divide the files, so every core can get almost the same quantity of data
i = 0
# Preparing the files
for f in text_files:
    if i < localP:
        send1.append(f)
        i = i + 1
    elif i < (2*localP):
        send2.append(f)
        i = i + 1
    elif i < (3*localP):
        send3.append(f)
        i = i + 1
    #elif i < (4*localP):
    #    send4.append(f)

#Send the files to each core so they can calculate the distance
if rank == 0:
    
    comm.send(send1,dest=0)
    comm.send(send2,dest=0)
    comm.send(send2,dest=1)
    comm.send(send3,dest=1)
    comm.send(send1,dest=2)
    comm.send(send3,dest=2)
    
    recv1 = comm.recv(source=0)
    recv2 = comm.recv(source=0)
    tst1 = []
    tst2 = []

    for f in recv1:
        d = open(f,'r', encoding=enc)
        tst1.append(d.read())
        d.close()
    for f in recv2:
        d = open(f,'r', encoding=enc)
        tst2.append(d.read())
        d.close()
    
    tfidf1 = (TfidfVectorizer().fit_transform(tst1)).toArray()
    tfidf2 = (TfidfVectorizer().fit_transform(tst2)).toArray()
    arr =[]

    file = open("out","w")
    file.write(str((tfidf1 * tfidf1.T).A))
    file.close()
                                                                
    
elif rank == 1:
    
    recv2 = comm.recv(source=0)
    recv3 = comm.recv(source=0)

    tst2 = []
    tst3 = []
    for f in recv2:
        d = open(f,'r', encoding=enc)
        tst2.append(d.read())
        d.close()
    for f in recv3:
        d = open(f,'r', encoding=enc)
        tst3.append(d.read())
        d.close()
                                
    tfidf2 = TfidfVectorizer().fit_transform(tst2)
    tfidf3 = TfidfVectorizer().fit_transform(tst3)

    comm.send(tfidf2,dest=0)
    comm.send(tfidf3,dest=0)
    
elif rank == 2:
    recv1 = comm.recv(source=0)
    recv3 = comm.recv(source=0) 
    tst1 = []
    tst3 = []
    for f in recv1:
        d = open(f,'r', encoding=enc)
        tst1.append(d.read())
        d.close()
    for f in recv3:
        d = open(f,'r', encoding=enc)
        tst3.append(d.read())
        d.close()
                                
    tfidf1 = TfidfVectorizer().fit_transform(tst1)
    tfidf3 = TfidfVectorizer().fit_transform(tst3)

    comm.send(tfidf1,dest=0)
    comm.send(tfidf3,dest=0)
        
