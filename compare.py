import glob
import numpy
from sklearn.feature_extraction.text import TfidfVectorizer
numpy.set_printoptions(threshold=numpy.nan)


text_files = glob.glob('Gutenberg/txt/*.txt')#Get all the files from Gutenberg
texts=[]
enc='latin-1'# Latin-1 is used for special character

for f in text_files:
    d = open(f,'r', encoding=enc) #Open all files 
    texts.append(d.read())
    d.close() 
 
tfidf = TfidfVectorizer().fit_transform(texts)#Calculate the distance between docs
res = tfidf * tfidf.T
file = open("out","w")#Create the out file
file.write(str(res.A))
file.close()#Close the created file
