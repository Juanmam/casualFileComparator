# Casual File Comparator

## About

Casual File Comparator is a HPC software meant to compare big sets of files to find out how similar they are, For this we implement TD-IDF it's a distance algorithm.It's intended to run on multiple nodes using MPI to reduce execution times.

## Requirements
* Pyton3
* MPI4Py
* Numpy
* Scipy
* Sklearn
* Glob

## How to use

#### Serial

```
$python3 serialcompare.py
```

#### Parallel
```
$mpiexec -n <Numbers Of Cores> python3 parallelCompare.py 
```
## Special thanks
* StackOverFlow
* Luis Miguel Mejía 
* Johan Sebatian Yepes 
* Juan David Pineda

## Authors
* Laura Sofia Muñoz
* 201310032010
* EAFIT University

* Juan Manuel Mejía
* 201320003010
* EAFIT University

* Camilo Zuluaga
* 201327000010
* EAFIT University